#include <stdio.h>

int input()
{
   int a;
   printf("Enter the number\n");
   scanf("%d",&a);
   return a;
}
int compute(int a)
{
   int i,n,count=0;
   for(i=2;i<=a;i++)
   {
       n=a%i;
       if (n==0)
       {
           count++;
       }
   }
   return count;
}
void output(int result)
{
   if (result>=2)
   {
      printf("Composite number\n");
   }
   else
   {
       printf("Prime number\n");
   }
}
int main()
{
   int x,y;
   x=input();
   y=compute(x);
   output(y);
   return 0;
}
