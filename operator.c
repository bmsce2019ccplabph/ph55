#include <stdio.h>

int main()
{
    int a,b,sum,difference,multiplication,modulus;
    float division;
    printf("Enter the values \n");
    scanf("%d%d",&a,&b);
    
    sum=a+b;
    difference=a-b;
    multiplication=a*b;
    division=(float)a/b;
    modulus=a%b;
    printf("sum of a and b is %d \n",sum);
    printf("difference of a and b is %d \n",difference);
    printf("multiplication of a and b is %d \n",multiplication);
    printf("division of a and b is %f \n",division);
    printf("modulus of a and b is %d \n",modulus);
    return 0;
}